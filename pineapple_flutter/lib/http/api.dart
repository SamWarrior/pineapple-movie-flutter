import 'package:pineapple_flutter/http/request.dart';

///
/// 接口调用工具类
///
class Api {
  //获取轮播
  static getBanner() {
    return Request.get("/client/banners/list");
  }

  //获取热门预告
  static getTrailers() {
    return Request.get("/client/trailers/list");
  }

  //获取猜你喜欢列表
  static getMovieList() {
    return Request.get("/client/movies/list");
  }

  //获取热门电影
  static getHotList() {
    return Request.get("/client/movies/hots");
  }

  //搜索
  static searchMovie(data) {
    return Request.get("/client/movies/search", data: data);
  }

  //登录
  static login(data) {
    return Request.post("/client/user/login", data: data);
  }

  //获取电影详情
  static getMovieDetail(data) {
    return Request.get("/client/movies/info", data: data);
  }

  //点赞
  static postLikes(data) {
    return Request.post("/client/movies/likes", data: data);
  }

  //收藏
  static postCollect(data) {
    return Request.post("/client/collections/postcolle", data: data);
  }

  //评论列表
  static getCommentList(data) {
    return Request.post("/client/comments/commentlist", data: data);
  }

  //发表评论
  static postComment(data) {
    return Request.post("/client/comments/sendcomment", data: data);
  }

  //获取个人信息
  static getUserinfo() {
    return Request.post("/client/user/userinfo");
  }

  //我的收藏
  static getMineCollect() {
    return Request.get("/client/collections/colleclist");
  }

  //我得评论
  static getMineComment() {
    return Request.get("/client/comments/mycomments");
  }

  //公告消息
  static getNoticeList() {
    return Request.get("/client/notices/list");
  }

  //修改密码
  static chngePwd(data) {
    return Request.post("/client/user/changepwd", data: data);
  }

  //修改个人资料
  static changeUserinfo(data) {
    return Request.post("/client/user/upuserinfo", data: data);
  }

  //意见反馈
  static postFeedback(data) {
    return Request.post("/client/feedback/sendfeed", data: data);
  }

  //关于信息
  static getAbout() {
    return Request.get("/client/about/aboutinfo");
  }


  //关于信息
  static upLoadImg(String imgPath) {
    return Request.fileUplod("/movie/api/upload",imgPath);
  }
}
