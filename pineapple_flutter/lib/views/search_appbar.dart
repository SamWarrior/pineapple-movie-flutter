import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';

class SearchAppBar extends StatefulWidget {
  SearchAppBar({Key? key, required this.hintLabel, Function? this.callBack})
      : super(key: key);

  final String hintLabel;
  var callBack;

  @override
  State<StatefulWidget> createState() {
    return SearchAppBarState();
  }
}

class SearchAppBarState extends State<SearchAppBar> {
  bool _offstage = true;

  final TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _textEditingController.addListener(() {
      var isVisible = _textEditingController.text.isNotEmpty;
      _updateDelIconVisible(isVisible);
    });
  }

  _updateDelIconVisible(bool isVisible) {
    setState(() {
      _offstage = !isVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 60.w,
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              height: double.infinity,
              margin: const EdgeInsets.only(left: 0),
              decoration: BoxDecoration(
                  color: Config.colorWhite,
                  borderRadius: BorderRadius.circular(30.w)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Padding(padding: EdgeInsets.only(left: 8)),
                  const Icon(
                    Icons.search,
                    color: Config.colorMain,
                    size: 20,
                  ),
                  const Padding(padding: EdgeInsets.only(left: 8)),
                  Expanded(
                    flex: 1,
                    child: TextField(
                      controller: _textEditingController,
                      autofocus: false,
                      style: TextStyle(fontSize: 22.w, color: Config.colorMain),
                      decoration: InputDecoration(
                          hintText: widget.hintLabel, border: InputBorder.none),
                      maxLines: 1,
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(left: 8)),
                  Offstage(
                    offstage: _offstage,
                    child: GestureDetector(
                      onTap: () => {_textEditingController.clear()},
                      child: const Icon(
                        Icons.close,
                        color: Config.colorMain,
                        size: 20,
                      ),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(left: 8)),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              widget.callBack(_textEditingController.text);
            },
            child: Container(
              padding: const EdgeInsets.only(left: 16, right: 5),
              child: Text("搜索",
                  style:
                      TextStyle(fontSize: 30.sp, color: (Config.colorText1))),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
