import 'package:chewie/chewie.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

class VideoController {
  //首页用的
  static getVideoController(String imgUrl, String movieUrl) {
    ChewieController chewieController = ChewieController(
      videoPlayerController: VideoPlayerController.network(movieUrl),
      // 比例
      aspectRatio: 700 / 350,
      // 自动播放
      autoPlay: false,
      // 循环
      looping: false,
      showOptions: false,
      allowMuting: false,
      placeholder: Image.network(
        imgUrl,
        fit: BoxFit.fill,
        width: 700.w,
        height: 350.w,
      ),
    );

    return chewieController;
  }

//电影详情页面用的
  static getVideoControllerForMovieDetail(String movieUrl) {
    ChewieController chewieController = ChewieController(
      videoPlayerController: VideoPlayerController.network(movieUrl),
      // 比例
      aspectRatio: 16 / 8.53,
      // 自动播放
      autoPlay: true,
      // 循环
      looping: false,
      showOptions: true,
      allowMuting: false,
    );

    return chewieController;
  }
}
