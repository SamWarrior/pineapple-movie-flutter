import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../config/config.dart';

///
/// dialog管理器
///
class DialogManager {

  ///默认样式的dialog
  static showDefaultDialog(
      BuildContext context, String desc, Function() onConfirm,
      {String title = '提示', String okText = '确定', String canText = '取消'}) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.NO_HEADER,
      borderSide: const BorderSide(
        color: Config.colorBg,
        width: 1,
      ),
      width: 660.w,
      buttonsBorderRadius: const BorderRadius.all(
        Radius.circular(5),
      ),
      dismissOnTouchOutside: false,
      dismissOnBackKeyPress: true,
      onDissmissCallback: (type) {},
      headerAnimationLoop: false,
      animType: AnimType.BOTTOMSLIDE,
      title: title,
      desc: desc,
      showCloseIcon: false,
      btnOkText: okText,
      btnCancelText: canText,
      btnCancelColor: Config.colorText3,
      btnOkColor: Config.colorMain,
      btnCancelOnPress: () {},
      btnOkOnPress: onConfirm,
    ).show();
  }
}
