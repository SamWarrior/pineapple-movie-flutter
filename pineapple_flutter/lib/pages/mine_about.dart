import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';
import 'package:pineapple_flutter/entity/mine_about_entity.dart';

import '../http/api.dart';

class MineAbout extends StatefulWidget {
  const MineAbout({Key? key}) : super(key: key);

  @override
  State<MineAbout> createState() => _MineAboutState();
}

class _MineAboutState extends State<MineAbout> {
  MineAboutEntity aboutEntity = MineAboutEntity();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    dynamic data = await Api.getAbout();
    setState(() {
      aboutEntity = MineAboutEntity.fromJson(data);
    });
  }

  getListItem(int index) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        border:
            Border(bottom: BorderSide(color: Config.colorDivider, width: 1.w)),
      ),
      height: 130.w,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Text(
              aboutEntity.data[index].title,
              softWrap: true,
              style: TextStyle(color: (Config.colorText1), fontSize: 25.w),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              aboutEntity.data[index].text,
              softWrap: false,
              textAlign: TextAlign.right,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: (Config.colorText1), fontSize: 25.w),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('关于我们'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        width: double.infinity,
        color: Config.colorBg,
        height: double.infinity,
        child: Stack(
          children: [
            Image.asset(
              'assets/images/login_bg.png',
              width: double.infinity,
              height: 300.w,
              fit: BoxFit.fill,
            ),
            Container(
              width: double.infinity,
              height: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 50.w, horizontal: 38.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  //大LOGO
                  Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child: Container(
                      margin: EdgeInsets.only(top: 30.w),
                      width: 152.w,
                      height: 152.w,
                      // padding: EdgeInsets.all(0.2.w),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Config.colorMain, // 底色
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 6, //阴影范围
                            spreadRadius: 2, //阴影浓度
                            color: Config.colorDivider, //阴影颜色
                            offset: Offset(4.w, 8.w),
                          ),
                        ],
                        borderRadius: BorderRadius.circular(73.w),
                      ),
                      child: const ClipOval(
                        child: SizedBox(
                          width: double.infinity,
                          height: double.infinity,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/launcher.png'),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 30.w)),
                  Text(
                    "熊猫电影-Flutter",
                    style: TextStyle(color: Config.colorWhite, fontSize: 30.w),
                  ),
                  Padding(padding: EdgeInsets.only(top: 5.w)),
                  Text(
                    "v1.0.0",
                    style: TextStyle(color: Config.colorWhite, fontSize: 25.w),
                  ),
                  Padding(padding: EdgeInsets.only(top: 30.w)),
                  SizedBox(
                    width: double.infinity,
                    height: 400.w,
                    child: ListView.builder(
                      itemCount: aboutEntity.data.length,
                      padding: EdgeInsets.all(30.w),
                      scrollDirection: Axis.vertical,
                      itemBuilder: (BuildContext context, int index) {
                        return getListItem(index);
                      },
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 480.w)),
                  Text(
                    "SamWarrior2022",
                    style: TextStyle(color: Config.colorWhite, fontSize: 22.w),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
