import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/entity/mine_comment_entity.dart';

import '../config/config.dart';
import '../entity/mine_collect_entity.dart';
import '../http/api.dart';
import '../routers/routers.dart';

class MineComment extends StatefulWidget {
  const MineComment({Key? key}) : super(key: key);

  @override
  State<MineComment> createState() => _MineCommentState();
}

class _MineCommentState extends State<MineComment> {
  MineCommentEntity dataEntity = MineCommentEntity();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    dynamic data = await Api.getMineComment();
    // Map<String, dynamic> result1 = Map.from(data);
    setState(() {
      dataEntity = MineCommentEntity.fromJson(data);
    });
  }

  getListItem(int index) {
    return InkWell(
      onTap: () {
        Routes.navigateTo(context, Routes.movieDetail,
            params: {'movieId': dataEntity.list[index].movieId});
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 20.w),
        decoration: BoxDecoration(
          color: (Config.colorMain),
          borderRadius: BorderRadius.circular(6),
        ),
        height: 320.w,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 16,
                child: Column(
                  children: [
                    SizedBox(
                      height: 205.w,
                      child: Text(
                        dataEntity.list[index].content,
                        softWrap: true,
                        style: TextStyle(
                            color: (Config.colorText1), fontSize: 25.w),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 15.w)),
                    const Divider(
                      height: 2,
                      thickness: 1,
                      color: Config.colorBg,
                    ),
                    Padding(padding: EdgeInsets.only(top: 12.w)),
                    Container(
                      width: double.infinity,
                      child: Text(
                        dataEntity.list[index].startTime,
                        softWrap: false,
                        textAlign: TextAlign.right,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: (Config.colorText1), fontSize: 25.w),
                      ),
                    ),
                  ],
                )),
            const Expanded(
              flex: 2,
              child: VerticalDivider(
                width: 2,
                thickness: 1,
                color: Config.colorBg,
              ),
            ),
            Expanded(
                flex: 6,
                child: Column(
                  children: [
                    ClipRRect(
                      //是ClipRRect，不是ClipRect
                      borderRadius: BorderRadius.circular(3),
                      child: Image.network(
                        "${Config.baseUrl}${dataEntity.list[index].cover}",
                        height: 220.w,
                        width: double.infinity,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 7.w)),
                    Text(
                      dataEntity.list[index].movieName,
                      softWrap: false,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(color: (Config.colorText1), fontSize: 25.w),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('我的评论'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        width: double.infinity,
        color: Config.colorBg,
        height: double.infinity,
        child: ListView.builder(
          itemCount: dataEntity.list.length,
          padding: EdgeInsets.all(20.w),
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return getListItem(index);
          },
        ),
      ),
    );
  }
}
