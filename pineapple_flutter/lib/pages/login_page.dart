import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/entity/user_info_entity.dart';
import 'package:pineapple_flutter/routers/routers.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config/config.dart';
import '../http/api.dart';
import '../provider/userinfo_provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  String _userName = "";
  String _passWord = "";

  ///
  /// 登录的逻辑
  ///
  login(Function onSucces) async {
    if (_userName.length < 11) {
      EasyLoading.showToast("请输入正确的手机号");
      return;
    }
    if (_passWord.length < 6) {
      EasyLoading.showToast("请输入6位以上密码");
      return;
    }

    var content = const Utf8Encoder().convert(_passWord);
    String md5pwd = "${md5.convert(content)}";
    dynamic result =
        await Api.login({'username': _userName, 'password': md5pwd});
    var jsonResult = jsonDecode(jsonEncode(result));
    if (jsonResult['code'] == 200) {
      //成功获得token,并保存起来
      SharedPreferences prefs = await SharedPreferences.getInstance();
      bool isOk = await prefs.setString('token', jsonResult['token']);
      // LogUtil.v(jsonResult['token']);
      if (isOk) {
        dynamic users = await Api.getUserinfo();
        Map<String, dynamic> resultUsers = Map.from(users);
        UserInfoEntity infoEntity = UserInfoEntity.fromJson(resultUsers);
        // onSucces.call();
        onSucces(infoEntity);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        color: Config.colorBg,
        height: double.infinity,
        child: Stack(
          children: [
            Image.asset(
              'assets/images/login_bg.png',
              width: double.infinity,
              height: 300.w,
              fit: BoxFit.fill,
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 50.w, horizontal: 38.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //返回按钮
                  Container(
                    margin: EdgeInsets.only(top: 20.w),
                    child: InkWell(
                      onTap: () {
                        Routes.router.pop(context);
                      },
                      child: const Icon(Icons.close),
                    ),
                  ),
                  //大LOGO
                  Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child: Container(
                      margin: EdgeInsets.only(top: 60.w),
                      width: 155.w,
                      height: 155.w,
                      // padding: EdgeInsets.all(0.2.w),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Config.colorMain, // 底色
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 6, //阴影范围
                            spreadRadius: 2, //阴影浓度
                            color: Config.colorDivider, //阴影颜色
                            offset: Offset(4.w, 8.w),
                          ),
                        ],
                        borderRadius: BorderRadius.circular(73.w),
                      ),
                      child: const ClipOval(
                        child: SizedBox(
                          width: double.infinity,
                          height: double.infinity,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/launcher.png'),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 78.w,
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 80.w, left: 60.w, right: 60.w),
                    child: TextField(
                      keyboardType: TextInputType.phone,
                      textAlign: TextAlign.justify,
                      maxLength: 11,
                      maxLines: 1,
                      onChanged: (value) {
                        setState(() {
                          _userName = value;
                        });
                      },
                      style:
                          TextStyle(fontSize: 26.sp, color: Config.colorText1),
                      decoration: InputDecoration(
                        hintText: '请输入手机号',
                        hintStyle: const TextStyle(color: Config.colorText3),
                        border: InputBorder.none,
                        counterText: '',
                        prefixIcon: Icon(
                          Icons.phone_android,
                          color: Config.colorWhite,
                          size: 33.w,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 0, horizontal: 60.w),
                    child: Divider(
                      height: 30.w,
                      color: Config.colorWhite,
                      thickness: 2.w,
                    ),
                  ),
                  Container(
                    height: 78.w,
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 10.w, left: 60.w, right: 60.w),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      textAlign: TextAlign.justify,
                      obscureText: true,
                      maxLength: 20,
                      maxLines: 1,
                      onChanged: (value) {
                        setState(() {
                          _passWord = value;
                        });
                      },
                      style:
                          TextStyle(fontSize: 26.sp, color: Config.colorText1),
                      decoration: InputDecoration(
                        hintText: '请输入密码',
                        counterText: '',
                        hintStyle: const TextStyle(color: Config.colorText3),
                        border: InputBorder.none,
                        prefixIcon: Icon(
                          Icons.lock_outline_rounded,
                          color: Config.colorWhite,
                          size: 33.w,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 0, horizontal: 60.w),
                    child: Divider(
                      height: 30.w,
                      color: Config.colorWhite,
                      thickness: 2.w,
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 85.w,
                    margin: EdgeInsets.fromLTRB(60.w, 80.w, 60.w, 0),
                    child: ElevatedButton(
                      clipBehavior: Clip.antiAlias,
                      onPressed: () => login((UserInfoEntity infoEntity) {
                        Provider.of<UserInfoProvider>(context, listen: false)
                            .changeUserinfo(infoEntity);
                        Routes.router.pop(context);
                      }),
                      style: ElevatedButton.styleFrom(
                          shape: const StadiumBorder()),
                      child: const Text('登录'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
