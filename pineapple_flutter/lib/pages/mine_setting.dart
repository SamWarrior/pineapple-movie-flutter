import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';

import '../routers/routers.dart';
import '../views/dialog_manager.dart';

class MineSetting extends StatefulWidget {
  const MineSetting({Key? key}) : super(key: key);

  @override
  State<MineSetting> createState() => _MineSettingState();
}

class _MineSettingState extends State<MineSetting> {
  ///
  /// 返回普通的列表item
  ///
  Widget getListItem(
      IconData icon, String title, String subTitle, Function() onTap) {
    return ListTile(
      onTap: onTap,
      leading: Icon(icon, size: 43.w),
      title: Text(title, style: TextStyle(fontSize: 25.w)),
      horizontalTitleGap: 0,
      dense: true,
      iconColor: Config.colorWhite,
      textColor: Config.colorWhite,
      trailing: SizedBox(
        width: 300.w,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(subTitle,
                style: TextStyle(fontSize: 23.w, color: Config.colorText3)),
            Padding(padding: EdgeInsets.all(10.w)),
            Icon(Icons.arrow_forward_ios, size: 20.w),
          ],
        ),
      ),
      contentPadding: EdgeInsets.symmetric(vertical: 9.w, horizontal: 0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('安全设置'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        //中间列表区
        width: double.infinity,
        height: 250.w,
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0.w),
        margin: EdgeInsets.symmetric(vertical: 30.w, horizontal: 30.w),
        decoration: BoxDecoration(
          color: (Config.colorMain),
          borderRadius: BorderRadius.circular(12.w),
        ),
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          padding: EdgeInsets.symmetric(vertical: 6.w, horizontal: 30.w),
          children: [
            getListItem(Icons.lock, "修改登录密码", "", () {
              Routes.router.navigateTo(context, Routes.mineUpPassword,
                  transition: TransitionType.inFromRight);
            }),
            const Divider(
              height: 0.5,
              color: Config.colorDivider,
            ),
            getListItem(Icons.cached, "清理缓存", "0KB", () {
              DialogManager.showDefaultDialog(context, "确认清理缓存吗", () {});
            }),
          ],
        ),
      ),
    );
  }
}
