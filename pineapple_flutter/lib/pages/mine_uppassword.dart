import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';

import '../http/api.dart';
import '../routers/routers.dart';

class MineUpPassword extends StatefulWidget {
  const MineUpPassword({Key? key}) : super(key: key);

  @override
  State<MineUpPassword> createState() => _MineUpPasswordState();
}

class _MineUpPasswordState extends State<MineUpPassword> {
  String oldPwd = "";
  String newPwd = "";
  String comfrim = "";

  ///
  /// 提交反馈内容
  ///
  senFeedBack(Function fun) async {
    if (oldPwd.isEmpty) {
      EasyLoading.showToast("请输入原密码");
      return;
    }

    if (newPwd.isEmpty) {
      EasyLoading.showToast("请输入新密码");
      return;
    }

    if (comfrim.isEmpty) {
      EasyLoading.showToast("请再次输入新密码");
      return;
    }

    if (newPwd != comfrim) {
      EasyLoading.showToast("两次密码不一致");
      return;
    }

    var oldp = const Utf8Encoder().convert(oldPwd);
    String md5OldPwd = "${md5.convert(oldp)}";
    var newp = const Utf8Encoder().convert(oldPwd);
    String md5NewPwd = "${md5.convert(newp)}";

    dynamic result =
        await Api.chngePwd({'oldpwd': md5OldPwd, 'newpwd': md5NewPwd});
    var jsonResult = jsonDecode(jsonEncode(result));
    if (jsonResult['code'] == 200) {
      EasyLoading.showToast("提交成功");
      fun();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('修改密码'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        margin: EdgeInsets.all(50.w),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 0.w),
              width: double.infinity,
              child: TextField(
                keyboardType: TextInputType.phone,
                textAlign: TextAlign.justify,
                maxLength: 20,
                obscureText: true,
                maxLines: 1,
                onChanged: (value) {
                  setState(() {
                    oldPwd = value;
                  });
                },
                style: TextStyle(fontSize: 26.sp, color: Config.colorText1),
                decoration: InputDecoration(
                  hintText: '请输入原密码',
                  hintStyle: const TextStyle(color: Config.colorText3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.w)),
                  ),
                  fillColor: Config.colorMain,
                  filled: true,
                  counterText: '',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.w),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.w),
              child: TextField(
                keyboardType: TextInputType.text,
                textAlign: TextAlign.justify,
                obscureText: true,
                maxLength: 20,
                maxLines: 1,
                onChanged: (value) {
                  setState(() {
                    newPwd = value;
                  });
                },
                style: TextStyle(fontSize: 26.sp, color: Config.colorText1),
                decoration: InputDecoration(
                  hintText: '请输入新密码',
                  counterText: '',
                  hintStyle: const TextStyle(color: Config.colorText3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.w)),
                  ),
                  fillColor: Config.colorMain,
                  filled: true,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.w),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.w),
              child: TextField(
                keyboardType: TextInputType.text,
                textAlign: TextAlign.justify,
                obscureText: true,
                maxLength: 20,
                maxLines: 1,
                onChanged: (value) {
                  setState(() {
                    comfrim = value;
                  });
                },
                style: TextStyle(fontSize: 26.sp, color: Config.colorText1),
                decoration: InputDecoration(
                  hintText: '请再次输入新密码',
                  counterText: '',
                  hintStyle: const TextStyle(color: Config.colorText3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.w)),
                  ),
                  fillColor: Config.colorMain,
                  filled: true,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 90.w,
              margin: EdgeInsets.only(top: 80.w),
              child: ElevatedButton(
                clipBehavior: Clip.antiAlias,
                onPressed: () => senFeedBack(() {
                  Routes.router.pop(context);
                }),
                style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
                child: const Text('确认修改'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
