import 'dart:convert';

import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../config/config.dart';
import '../entity/user_info_entity.dart';
import '../http/api.dart';
import '../provider/userinfo_provider.dart';
import '../routers/routers.dart';

class MoviePostComment extends StatefulWidget {
  String movieId;

  MoviePostComment({Key? key, this.movieId = ""}) : super(key: key);

  @override
  State<MoviePostComment> createState() => _MoviePostCommentState();
}

class _MoviePostCommentState extends State<MoviePostComment> {
  String _content = "";

  ///
  /// 提交反馈内容
  ///
  onClick() async {
    if (_content.isEmpty) {
      EasyLoading.showToast("请输入评论内容");
      return;
    }
    postSubmit();
  }

  ///
  /// 提交
  ///
  postSubmit() async {
    dynamic result =
        await Api.postComment({'movieId': widget.movieId, "content": _content});
    var jsonResult = jsonDecode(jsonEncode(result));
    if (jsonResult['code'] == 200) {
      EasyLoading.showToast("提交成功");
      Routes.router.pop(context, {"result": 1});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('发表评论'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        margin: EdgeInsets.all(55.w),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.only(top: 20.w),
              child: TextField(
                keyboardType: TextInputType.text,
                textAlign: TextAlign.justify,
                obscureText: false,
                maxLength: 200,
                maxLines: 10,
                onChanged: (value) {
                  setState(() {
                    _content = value;
                  });
                },
                style: TextStyle(fontSize: 28.w, color: Config.colorText1),
                decoration: InputDecoration(
                  hintText: '请输入评论内容',
                  counterText: '',
                  hintStyle: const TextStyle(color: Config.colorText3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25.w)),
                  ),
                  fillColor: Config.colorMain,
                  filled: true,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 90.w,
              margin: EdgeInsets.symmetric(vertical: 0.w, horizontal: 0.w),
              child: ElevatedButton(
                clipBehavior: Clip.antiAlias,
                onPressed: () => onClick(),
                style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
                child: const Text('提交'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
