import 'package:flutter/material.dart';
import 'package:pineapple_flutter/routers/routers.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SplashPage();
  }
}

class _SplashPage extends State<SplashPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(const Duration(milliseconds: 2500), () {
      // Navigator.pushReplacement(
      //     context, MaterialPageRoute(builder: (context) => const HomePage()));
      Routes.router.navigateTo(context, Routes.home, replace: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Image.asset('assets/images/bg_welcome_full.png', fit: BoxFit.fill),
    );
  }
}
