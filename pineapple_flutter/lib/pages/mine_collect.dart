import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';
import 'package:pineapple_flutter/entity/mine_collect_entity.dart';

import '../entity/movie_list_entity.dart';
import '../http/api.dart';
import '../routers/routers.dart';

class MineCollect extends StatefulWidget {
  const MineCollect({Key? key}) : super(key: key);

  @override
  State<MineCollect> createState() => _MineCollectState();
}

class _MineCollectState extends State<MineCollect> {
  MineCollectEntity mineCollectEntity = MineCollectEntity();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    dynamic data = await Api.getMineCollect();
    // Map<String, dynamic> result1 = Map.from(data);
    setState(() {
      mineCollectEntity = MineCollectEntity.fromJson(data);
    });
  }

  getListItem(String movieId, String imgurl, String title) {
    return InkWell(
      onTap: () {
        Routes.navigateTo(context, Routes.movieDetail,
            params: {'movieId': movieId});
      },
      child: Container(
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: (Config.colorMain),
          borderRadius: BorderRadius.circular(6),
        ),
        width: 210.w,
        height: 280.w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
              //是ClipRRect，不是ClipRect
              borderRadius: BorderRadius.circular(3),
              child: Image.network(
                imgurl,
                height: 268.w,
                width: double.infinity,
                fit: BoxFit.fill,
              ),
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 6.w)),
            Text(
              title,
              softWrap: false,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: (Config.colorText1), fontSize: 28.w),
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 2.w)),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('我的收藏'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        width: double.infinity,
        color: Config.colorBg,
        height: double.infinity,
        child: GridView.builder(
          itemCount: mineCollectEntity.list.length,
          padding: EdgeInsets.all(20.w),
          scrollDirection: Axis.vertical,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            childAspectRatio: 0.64,
          ),
          itemBuilder: (BuildContext context, int index) {
            return getListItem(
                mineCollectEntity.list[index].movieId,
                "${Config.baseUrl}${mineCollectEntity.list[index].cover}",
                mineCollectEntity.list[index].movieName);
          },
        ),
      ),
    );
  }
}
