import 'dart:convert';

import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';

import '../http/api.dart';
import '../routers/routers.dart';

class MineFeedback extends StatefulWidget {
  const MineFeedback({Key? key}) : super(key: key);

  @override
  State<MineFeedback> createState() => _MineFeedbackState();
}

class _MineFeedbackState extends State<MineFeedback> {
  String _title = "";
  String _content = "";


  ///
  /// 提交反馈内容
  ///
  senFeedBack(Function fun) async {
    if (_title.isEmpty) {
      EasyLoading.showToast("请输入反馈标题");
      return;
    }

    if (_content.isEmpty) {
      EasyLoading.showToast("请输入反馈内容");
      return;
    }

    dynamic result =
        await Api.postFeedback({'title': _title, 'content': _content});
    var jsonResult = jsonDecode(jsonEncode(result));
    if (jsonResult['code'] == 200) {
      EasyLoading.showToast("提交成功");
      fun();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('意见反馈'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        margin: EdgeInsets.all(55.w),
        child: Column(
          children: [
            SizedBox(
              width: double.infinity,
              child: Text(
                '标题',
                style: TextStyle(
                  color: Config.colorWhite,
                  fontSize: 30.w,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.w),
              width: double.infinity,
              child: TextField(
                keyboardType: TextInputType.phone,
                textAlign: TextAlign.justify,
                maxLength: 20,
                maxLines: 1,
                onChanged: (value) {
                  setState(() {
                    _title = value;
                  });
                },
                style: TextStyle(fontSize: 26.sp, color: Config.colorText1),
                decoration: InputDecoration(
                  hintText: '请输入反馈标题',
                  hintStyle: const TextStyle(color: Config.colorText3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.w)),
                  ),
                  fillColor: Config.colorMain,
                  filled: true,
                  counterText: '',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 40.w),
            ),
            SizedBox(
              width: double.infinity,
              child: Text(
                '内容',
                style: TextStyle(
                  color: Config.colorWhite,
                  fontSize: 30.w,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.w),
              child: TextField(
                keyboardType: TextInputType.text,
                textAlign: TextAlign.justify,
                obscureText: false,
                maxLength: 200,
                maxLines: 10,
                onChanged: (value) {
                  setState(() {
                    _content = value;
                  });
                },
                style: TextStyle(fontSize: 26.sp, color: Config.colorText1),
                decoration: InputDecoration(
                  hintText: '请输入反馈内容',
                  counterText: '',
                  hintStyle: const TextStyle(color: Config.colorText3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.w)),
                  ),
                  fillColor: Config.colorMain,
                  filled: true,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 90.w,
              margin: EdgeInsets.symmetric(vertical: 100.w, horizontal: 0.w),
              child: ElevatedButton(
                clipBehavior: Clip.antiAlias,
                onPressed: () => senFeedBack(() {
                  Routes.router.pop(context);
                }),
                style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
                child: const Text('提交'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
