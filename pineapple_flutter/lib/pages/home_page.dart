import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';
import 'package:pineapple_flutter/tabview/home_index.dart';
import 'package:pineapple_flutter/tabview/home_mine.dart';
import 'package:pineapple_flutter/tabview/home_search.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<TabItem> tabItems = [
    const TabItem(icon: Icons.home, title: '首页'),
    const TabItem(icon: Icons.search, title: '搜索'),
    const TabItem(icon: Icons.person, title: '我的'),
  ];

  List<Widget> tabViews = [
    const HomeIndex(),
    const HomeSearch(),
    const HomeMine(),
  ];

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: tabViews[_currentIndex],

      body: IndexedStack(
        index: _currentIndex,
        children: tabViews,
      ),

      bottomNavigationBar: ConvexAppBar(
        items: tabItems,
        backgroundColor: Config.colorMain,
        color: Colors.white,
        activeColor: Colors.white,
        style: TabStyle.reactCircle,
        elevation: 6.w,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
