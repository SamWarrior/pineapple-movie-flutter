import 'dart:convert';

import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../config/config.dart';
import '../entity/user_info_entity.dart';
import '../http/api.dart';
import '../provider/userinfo_provider.dart';
import '../routers/routers.dart';

class MineUpNickname extends StatefulWidget {
  const MineUpNickname({Key? key}) : super(key: key);

  @override
  State<MineUpNickname> createState() => _MineUpNickNameState();
}

class _MineUpNickNameState extends State<MineUpNickname> {
  String _content = "";
  late UserInfoEntity infoEntity;

  ///
  /// 提交反馈内容
  ///
  onClick() async {
    if (_content.isEmpty) {
      EasyLoading.showToast("请输入新昵称");
      return;
    }
    infoEntity.user.nickname = _content;
    upUserinfo();
  }

  ///
  /// 更新个人信息
  ///
  upUserinfo() async {
    Map a = {'userinfo': Map.from(infoEntity.user.toJson())};
    dynamic result = await Api.changeUserinfo(a);
    var jsonResult = jsonDecode(jsonEncode(result));
    if (jsonResult['code'] == 200) {
      EasyLoading.showToast("提交成功");
      Provider.of<UserInfoProvider>(context, listen: false)
          .changeUserinfo(infoEntity);
      Routes.router.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    infoEntity = Provider.of<UserInfoProvider>(context).userInfo;
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('修改昵称'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        margin: EdgeInsets.all(55.w),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.only(top: 20.w),
              child: TextField(
                keyboardType: TextInputType.text,
                textAlign: TextAlign.justify,
                obscureText: false,
                maxLength: 10,
                maxLines: 1,
                onChanged: (value) {
                  setState(() {
                    _content = value;
                  });
                },
                style: TextStyle(fontSize: 28.w, color: Config.colorText1),
                decoration: InputDecoration(
                  hintText: '请输入新昵称',
                  counterText: '',
                  hintStyle: const TextStyle(color: Config.colorText3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25.w)),
                  ),
                  fillColor: Config.colorMain,
                  filled: true,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 90.w,
              margin: EdgeInsets.symmetric(vertical: 0.w, horizontal: 0.w),
              child: ElevatedButton(
                clipBehavior: Clip.antiAlias,
                onPressed: () => onClick(),
                style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
                child: const Text('提交'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
