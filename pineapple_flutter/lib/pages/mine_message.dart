import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/entity/mine_message_entity.dart';

import '../config/config.dart';
import '../http/api.dart';

class MineMessage extends StatefulWidget {
  const MineMessage({Key? key}) : super(key: key);

  @override
  State<MineMessage> createState() => _MineMessageState();
}

class _MineMessageState extends State<MineMessage> {
  MineMessageEntity dataEntity = MineMessageEntity();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    dynamic data = await Api.getNoticeList();
    // Map<String, dynamic> result1 = Map.from(data);
    setState(() {
      dataEntity = MineMessageEntity.fromJson(data);
    });
  }

  getListItem(int index) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 20.w),
      decoration: BoxDecoration(
        color: (Config.colorMain),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Column(
        children: [
          SizedBox(
            child: Text(
              dataEntity.list[index].content,
              softWrap: true,
              style: TextStyle(color: (Config.colorText1), fontSize: 25.w),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 20.w)),
          const Divider(
            height: 2,
            thickness: 1,
            color: Config.colorBg,
          ),
          Padding(padding: EdgeInsets.only(top: 12.w)),
          SizedBox(
            width: double.infinity,
            child: Text(
              dataEntity.list[index].startTime,
              softWrap: false,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: (Config.colorText1), fontSize: 25.w),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.colorBg,
      appBar: AppBar(
        title: const Text('消息中心'),
        centerTitle: true,
        backgroundColor: Config.colorMain,
      ),
      body: Container(
        width: double.infinity,
        color: Config.colorBg,
        height: double.infinity,
        child: ListView.builder(
          itemCount: dataEntity.list.length,
          padding: EdgeInsets.all(25.w),
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return getListItem(index);
          },
        ),
      ),
    );
  }
}
