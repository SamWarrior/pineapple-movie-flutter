import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:pineapple_flutter/pages/home_page.dart';
import 'package:pineapple_flutter/pages/login_page.dart';
import 'package:pineapple_flutter/pages/mine_about.dart';
import 'package:pineapple_flutter/pages/mine_collect.dart';
import 'package:pineapple_flutter/pages/mine_comment.dart';
import 'package:pineapple_flutter/pages/mine_feedback.dart';
import 'package:pineapple_flutter/pages/mine_info.dart';
import 'package:pineapple_flutter/pages/mine_message.dart';
import 'package:pineapple_flutter/pages/mine_setting.dart';
import 'package:pineapple_flutter/pages/mine_upnickname.dart';
import 'package:pineapple_flutter/pages/mine_uppassword.dart';
import 'package:pineapple_flutter/pages/movie_details.dart';
import 'package:pineapple_flutter/pages/movie_postcomment.dart';

import '../pages/photo_view.dart';

///
/// fluro路由的跳转动作封装
///

// 跳转首页
Handler homePageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const HomePage();
});

// 跳转登录页
Handler loginPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const LoginPage();
});

// 跳转电影详情页
Handler movieDetailHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  String? movieId = parameters['movieId']?.first;
  return MovieDetails(
    movieId: movieId ?? '',
  );
});

// 跳转到我的收藏
Handler mineCollectHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineCollect();
});

// 跳转到我的评论
Handler mineCommentHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineComment();
});

// 跳转到消息中心
Handler mineMessageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineMessage();
});

// 跳转到个人中心
Handler mineInfoHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineInfo();
});

// 跳转到安全设置
Handler mineSettingHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineSetting();
});

// 跳转到意见反馈
Handler mineFeedbackHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineFeedback();
});

// 跳转到关于我们
Handler mineAboutHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineAbout();
});

// 跳转到修改昵称
Handler mineUpNicknameHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineUpNickname();
});

// 跳转到发表评论
Handler moviePostCommentHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  String? movieId = parameters['movieId']?.first;
  return MoviePostComment(
    movieId: movieId ?? '',
  );
});

// 跳转到大图浏览
Handler photoPreviewHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  String? defaultImage = parameters['defaultImage']?.first;
  defaultImage ??= '0';
  return PhotoPreview(
    defaultImage: int.parse(defaultImage),
  );
});

// 跳转到修改密码
Handler mineUpPasswordHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return const MineUpPassword();
});

//找不到路由
Handler notFoundHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  print('页面找不到404');
  return null;
});
