import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import './router_handler.dart';

///
/// fluro路由的总配置
///
class Routes {
  static late FluroRouter router;

  //默认路由（APP首页）
  static String home = '/';
  static String login = '/login';
  static String movieDetail = '/movie_detail';
  static String mineCollect = '/mine_collect';
  static String mineComment = '/mine_comment';
  static String mineMessage = '/mine_message';
  static String mineInfo = '/mine_info';
  static String mineSetting = '/mine_setting';
  static String mineFeedback = '/mine_feedback';
  static String mineAbout = '/mine_about';
  static String mineUpNickname = '/mine_up_nickname';
  static String moviePostComment = '/mive_post_comment';
  static String photoPreview = '/photo_preview';
  static String mineUpPassword = '/mine_up_password';

  static void initRouter() {
    router = FluroRouter();
    defineRoutes();
  }

  ///
  /// 路由的路径和动作一对一绑定
  ///
  static void defineRoutes() {
    router.define(home, handler: homePageHandler);
    router.define(login, handler: loginPageHandler);
    router.define(movieDetail, handler: movieDetailHandler);
    router.define(mineCollect, handler: mineCollectHandler);
    router.define(mineComment, handler: mineCommentHandler);
    router.define(mineMessage, handler: mineMessageHandler);
    router.define(mineInfo, handler: mineInfoHandler);
    router.define(mineSetting, handler: mineSettingHandler);
    router.define(mineFeedback, handler: mineFeedbackHandler);
    router.define(mineAbout, handler: mineAboutHandler);
    router.define(mineUpNickname, handler: mineUpNicknameHandler);
    router.define(moviePostComment, handler: moviePostCommentHandler);
    router.define(photoPreview, handler: photoPreviewHandler);
    router.define(mineUpPassword, handler: mineUpPasswordHandler);

    router.notFoundHandler = notFoundHandler;
  }

  // 对参数进行encode，解决参数中有特殊字符，影响fluro路由匹配
  static Future navigateTo(BuildContext context, String path,
      {Map<String, dynamic>? params,
      TransitionType transition = TransitionType.inFromRight}) {
    String query = "";
    if (params != null) {
      int index = 0;
      for (var key in params.keys) {
        var value = Uri.encodeComponent(params[key]);
        if (index == 0) {
          query = "?";
        } else {
          query = "$query&";
        }
        query += "$key=$value";
        index++;
      }
    }

    path = path + query;
    print('我是navigateTo跳转：$path');
    return router.navigateTo(context, path, transition: transition);
  }
}
