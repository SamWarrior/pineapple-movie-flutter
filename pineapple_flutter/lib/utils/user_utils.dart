import 'package:common_utils/common_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:pineapple_flutter/entity/user_info_entity.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../provider/userinfo_provider.dart';

class UserUtils {
  static loginOut(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("token");
    Provider.of<UserInfoProvider>(context, listen: false)
        .changeUserinfo(UserInfoEntity());
  }

  static isLogin(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString("token");
    if (token != null) {
      return true;
    } else {
      return false;
    }
  }
}
