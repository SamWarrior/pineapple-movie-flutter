///单例类
class SingletonManager {
  // 工厂方法构造函数,通过SingletonManager()获取对象
  factory SingletonManager() => _getInstance();

  // instance的getter方法 - 通过SingletonManager.instance获取对象
  static SingletonManager get instance => _getInstance();

  // 静态变量_instance，存储唯一对象
  static SingletonManager? _instance;

  // 获取唯一对象
  static SingletonManager _getInstance() {
    _instance ??= SingletonManager._internal();
    return _instance!;
  }

  //初始化...
  SingletonManager._internal() {
    //初始化其他操作...
  }

  List galleryItems = [];
}
