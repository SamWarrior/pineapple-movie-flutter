import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/mine_comment_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MineCommentEntity {
  late int code = 0;
  late String msg;
  late List<MineCommentList> list = [];

  MineCommentEntity();

  factory MineCommentEntity.fromJson(Map<String, dynamic> json) =>
      $MineCommentEntityFromJson(json);

  Map<String, dynamic> toJson() => $MineCommentEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MineCommentList {
  @JSONField(name: "_id")
  late String sId;
  late String movieId;
  late String customid;
  late String content;
  late String startTime;
  @JSONField(name: "__v")
  late int iV;
  late String movieName;
  late String cover;

  MineCommentList();

  factory MineCommentList.fromJson(Map<String, dynamic> json) =>
      $MineCommentListFromJson(json);

  Map<String, dynamic> toJson() => $MineCommentListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
