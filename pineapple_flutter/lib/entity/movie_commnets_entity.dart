import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/movie_commnets_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MovieCommnetsEntity {

	late int code=0;
	late String msg;
	late List<MovieCommnetsList> list=[];
  
  MovieCommnetsEntity();

  factory MovieCommnetsEntity.fromJson(Map<String, dynamic> json) => $MovieCommnetsEntityFromJson(json);

  Map<String, dynamic> toJson() => $MovieCommnetsEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MovieCommnetsList {

	@JSONField(name: "_id")
	late String sId;
	late String movieId;
	late String customid;
	late String content='';
	late String startTime='';
	@JSONField(name: "__v")
	late int iV;
	late String headicon='';
	late String nickname='';
  
  MovieCommnetsList();

  factory MovieCommnetsList.fromJson(Map<String, dynamic> json) => $MovieCommnetsListFromJson(json);

  Map<String, dynamic> toJson() => $MovieCommnetsListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}