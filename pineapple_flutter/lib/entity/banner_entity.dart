import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/banner_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class BannerEntity {
  late int code=0;
  late String msg;
  late List<BannerList> list = [];

  BannerEntity();

  factory BannerEntity.fromJson(Map<String, dynamic> json) =>
      $BannerEntityFromJson(json);

  Map<String, dynamic> toJson() => $BannerEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class BannerList {
  @JSONField(name: "_id")
  late String sId;
  late String movieId;
  late String movieName;
  late String imageUrl;
  @JSONField(name: "__v")
  late int iV;

  BannerList();

  factory BannerList.fromJson(Map<String, dynamic> json) =>
      $BannerListFromJson(json);

  Map<String, dynamic> toJson() => $BannerListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
