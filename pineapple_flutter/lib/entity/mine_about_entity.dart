import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/mine_about_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MineAboutEntity {
  late int code = 0;
  late String msg;
  late List<MineAboutData> data = [];

  MineAboutEntity();

  factory MineAboutEntity.fromJson(Map<String, dynamic> json) =>
      $MineAboutEntityFromJson(json);

  Map<String, dynamic> toJson() => $MineAboutEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MineAboutData {
  late String title;
  late String text;

  MineAboutData();

  factory MineAboutData.fromJson(Map<String, dynamic> json) =>
      $MineAboutDataFromJson(json);

  Map<String, dynamic> toJson() => $MineAboutDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
