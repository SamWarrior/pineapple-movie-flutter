import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/trailer_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class TrailerEntity {

	late int code=0;
	late String msg;
	late List<TrailerList> list=[];
  
  TrailerEntity();

  factory TrailerEntity.fromJson(Map<String, dynamic> json) => $TrailerEntityFromJson(json);

  Map<String, dynamic> toJson() => $TrailerEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class TrailerList {

	@JSONField(name: "_id")
	late String sId;
	late String movieId;
	late String movieName;
	late String movieUrl;
	late String cover;
	@JSONField(name: "__v")
	late int iV;
  
  TrailerList();

  factory TrailerList.fromJson(Map<String, dynamic> json) => $TrailerListFromJson(json);

  Map<String, dynamic> toJson() => $TrailerListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}