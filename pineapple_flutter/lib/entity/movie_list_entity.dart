import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/movie_list_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MovieListEntity {
  late int code = 0;
  late String msg;
  late List<MovieListList> list = [];

  MovieListEntity();

  factory MovieListEntity.fromJson(Map<String, dynamic> json) =>
      $MovieListEntityFromJson(json);

  Map<String, dynamic> toJson() => $MovieListEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MovieListList {
  late double score = 5.0;
  late int counts;
  @JSONField(name: "_id")
  late String sId;
  late List<MovieListListPlotsWords> plotsWords;
  late List<MovieListListPlotsImg> plotsImg;
  late String movieId;
  late String movieName;
  late String originaName;
  late String info;
  late String releaseDate;
  late String totalTime;
  late String movieUrl;
  late String plotDesc;
  late String cover;
  @JSONField(name: "__v")
  late int iV;

  MovieListList();

  factory MovieListList.fromJson(Map<String, dynamic> json) =>
      $MovieListListFromJson(json);

  Map<String, dynamic> toJson() => $MovieListListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MovieListListPlotsWords {
  @JSONField(name: "_id")
  late String sId;
  late String imgUrl;
  late String autor;
  late String actorRole;

  MovieListListPlotsWords();

  factory MovieListListPlotsWords.fromJson(Map<String, dynamic> json) =>
      $MovieListListPlotsWordsFromJson(json);

  Map<String, dynamic> toJson() => $MovieListListPlotsWordsToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MovieListListPlotsImg {
  @JSONField(name: "_id")
  late String sId;
  late String imgUrl;

  MovieListListPlotsImg();

  factory MovieListListPlotsImg.fromJson(Map<String, dynamic> json) =>
      $MovieListListPlotsImgFromJson(json);

  Map<String, dynamic> toJson() => $MovieListListPlotsImgToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
