import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/movie_detail_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MovieDetailEntity {
  late int code = 0;
  late String msg;
  late MovieDetailItem item = MovieDetailItem();

  MovieDetailEntity();

  factory MovieDetailEntity.fromJson(Map<String, dynamic> json) =>
      $MovieDetailEntityFromJson(json);

  Map<String, dynamic> toJson() => $MovieDetailEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MovieDetailItem {
  late double score = 8;
  late int counts = 0;
  @JSONField(name: "_id")
  late String sId;
  late List<MovieDetailItemPlotsWords> plotsWords = [];
  late List<MovieDetailItemPlotsImg> plotsImg = [];
  late String movieId;
  late String movieName = '';
  late String originaName = '';
  late String info = '';
  late String releaseDate = '';
  late String totalTime = '';
  late String movieUrl = '';
  late String plotDesc = '';
  late String cover = '';
  @JSONField(name: "__v")
  late int iV;
  late String iscollec = "0";

  MovieDetailItem();

  factory MovieDetailItem.fromJson(Map<String, dynamic> json) =>
      $MovieDetailItemFromJson(json);

  Map<String, dynamic> toJson() => $MovieDetailItemToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MovieDetailItemPlotsWords {
  @JSONField(name: "_id")
  late String sId;
  late String imgUrl;
  late String autor;
  late String actorRole;

  MovieDetailItemPlotsWords();

  factory MovieDetailItemPlotsWords.fromJson(Map<String, dynamic> json) =>
      $MovieDetailItemPlotsWordsFromJson(json);

  Map<String, dynamic> toJson() => $MovieDetailItemPlotsWordsToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MovieDetailItemPlotsImg {
  @JSONField(name: "_id")
  late String sId;
  late String imgUrl = '';

  MovieDetailItemPlotsImg();

  factory MovieDetailItemPlotsImg.fromJson(Map<String, dynamic> json) =>
      $MovieDetailItemPlotsImgFromJson(json);

  Map<String, dynamic> toJson() => $MovieDetailItemPlotsImgToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
