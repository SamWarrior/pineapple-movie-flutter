import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/hot_movie_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class HotMovieEntity {
  late int code = 0;
  late String msg;
  late List<HotMovieHots> hots = [];

  HotMovieEntity();

  factory HotMovieEntity.fromJson(Map<String, dynamic> json) =>
      $HotMovieEntityFromJson(json);

  Map<String, dynamic> toJson() => $HotMovieEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class HotMovieHots {
  late double score = 5;
  late int counts;
  @JSONField(name: "_id")
  late String sId;
  late List<HotMovieHotsPlotsWords> plotsWords;
  late List<HotMovieHotsPlotsImg> plotsImg;
  late String movieId;
  late String movieName;
  late String originaName;
  late String info;
  late String releaseDate;
  late String totalTime;
  late String movieUrl;
  late String plotDesc;
  late String cover;
  @JSONField(name: "__v")
  late int iV;

  HotMovieHots();

  factory HotMovieHots.fromJson(Map<String, dynamic> json) =>
      $HotMovieHotsFromJson(json);

  Map<String, dynamic> toJson() => $HotMovieHotsToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class HotMovieHotsPlotsWords {
  @JSONField(name: "_id")
  late String sId;
  late String imgUrl;
  late String actorRole;
  late String autor;

  HotMovieHotsPlotsWords();

  factory HotMovieHotsPlotsWords.fromJson(Map<String, dynamic> json) =>
      $HotMovieHotsPlotsWordsFromJson(json);

  Map<String, dynamic> toJson() => $HotMovieHotsPlotsWordsToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class HotMovieHotsPlotsImg {
  @JSONField(name: "_id")
  late String sId;
  late String imgUrl;

  HotMovieHotsPlotsImg();

  factory HotMovieHotsPlotsImg.fromJson(Map<String, dynamic> json) =>
      $HotMovieHotsPlotsImgFromJson(json);

  Map<String, dynamic> toJson() => $HotMovieHotsPlotsImgToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
