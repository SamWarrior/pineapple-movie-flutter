import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/mine_collect_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MineCollectEntity {
  late int code = 0;
  late String msg;
  late List<MineCollectList> list = [];

  MineCollectEntity();

  factory MineCollectEntity.fromJson(Map<String, dynamic> json) =>
      $MineCollectEntityFromJson(json);

  Map<String, dynamic> toJson() => $MineCollectEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MineCollectList {
  @JSONField(name: "_id")
  late String sId;
  late String customid;
  late String movieId;
  late String movieName;
  late String cover;
  @JSONField(name: "__v")
  late int iV;

  MineCollectList();

  factory MineCollectList.fromJson(Map<String, dynamic> json) =>
      $MineCollectListFromJson(json);

  Map<String, dynamic> toJson() => $MineCollectListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
