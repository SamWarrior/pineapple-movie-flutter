import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/user_info_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class UserInfoEntity {
  late int code = 0;
  late String msg;
  late UserInfoUser user;

  UserInfoEntity();

  factory UserInfoEntity.fromJson(Map<String, dynamic> json) =>
      $UserInfoEntityFromJson(json);

  Map<String, dynamic> toJson() => $UserInfoEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class UserInfoUser {
  late String headicon;
  late String nickname;
  late String address;
  @JSONField(name: "_id")
  late String sId;
  late String username;
  late String password;
  @JSONField(name: "__v")
  late int iV;

  UserInfoUser();

  factory UserInfoUser.fromJson(Map<String, dynamic> json) =>
      $UserInfoUserFromJson(json);

  Map<String, dynamic> toJson() => $UserInfoUserToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
