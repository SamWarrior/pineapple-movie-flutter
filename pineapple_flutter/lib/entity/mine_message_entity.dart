import 'package:pineapple_flutter/generated/json/base/json_field.dart';
import 'package:pineapple_flutter/generated/json/mine_message_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MineMessageEntity {
  late int code = 0;
  late String msg;
  late List<MineMessageList> list = [];

  MineMessageEntity();

  factory MineMessageEntity.fromJson(Map<String, dynamic> json) =>
      $MineMessageEntityFromJson(json);

  Map<String, dynamic> toJson() => $MineMessageEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MineMessageList {
  @JSONField(name: "_id")
  late String sId;
  late String title;
  late String content;
  late String startTime;
  @JSONField(name: "__v")
  late int iV;

  MineMessageList();

  factory MineMessageList.fromJson(Map<String, dynamic> json) =>
      $MineMessageListFromJson(json);

  Map<String, dynamic> toJson() => $MineMessageListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
