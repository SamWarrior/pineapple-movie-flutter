import 'dart:ui';

class Config {
  //网络配置
  //=========================================================
  static const String baseUrl = "http://101.33.206.34:3030";
  // static const String baseUrl = "http://192.168.1.11:3030";

  //颜色配置
  //=========================================================
  static const Color colorBg = Color(0xFF121325);
  static const Color colorMain = Color(0xFF121330);
  static const Color colorWhite = Color(0xFFFFFFFF);
  static const Color colorDivider = Color(0xFF020202);
  static const Color colorText1 = Color(0xFFFFFFFF);
  static const Color colorText2 = Color(0xFF999999);
  static const Color colorText3 = Color(0xFF999999);
}
