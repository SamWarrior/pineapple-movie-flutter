import 'package:flutter/material.dart';
import 'package:pineapple_flutter/entity/movie_list_entity.dart';
import 'package:pineapple_flutter/views/search_appbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../config/config.dart';
import '../http/api.dart';
import '../routers/routers.dart';

class HomeSearch extends StatefulWidget {
  const HomeSearch({Key? key}) : super(key: key);

  @override
  State<HomeSearch> createState() => _HomeSearchState();
}

class _HomeSearchState extends State<HomeSearch> {
  String keywords = "";
  MovieListEntity listEntity = MovieListEntity();

  getListItem(String movieId, String imgurl, String title) {
    return InkWell(
      onTap: () {
        Routes.navigateTo(context, Routes.movieDetail,
            params: {'movieId': movieId});
      },
      child: Container(
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: (Config.colorMain),
          borderRadius: BorderRadius.circular(6),
        ),
        width: 210.w,
        height: 280.w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
              //是ClipRRect，不是ClipRect
              borderRadius: BorderRadius.circular(3),
              child: Image.network(
                imgurl,
                height: 268.w,
                width: double.infinity,
                fit: BoxFit.fill,
              ),
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 6.w)),
            Text(
              title,
              softWrap: false,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: (Config.colorText1), fontSize: 28.w),
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 2.w)),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    dynamic searchList = await Api.searchMovie({'keywords': keywords});
    Map<String, dynamic> result1 = Map.from(searchList);
    setState(() {
      listEntity = MovieListEntity.fromJson(result1);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.colorMain,
        title: SearchAppBar(
          hintLabel: '请输入关键字',
          callBack: (keyword) {
            keywords = keyword;
            getData();
          },
        ),
        elevation: 0.6,
        centerTitle: true,
      ),
      body: Container(
        width: double.infinity,
        color: Config.colorBg,
        height: double.infinity,
        child: GridView.builder(
          itemCount: listEntity.list.length,
          padding: EdgeInsets.all(20.w),
          scrollDirection: Axis.vertical,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            childAspectRatio: 0.64,
          ),
          itemBuilder: (BuildContext context, int index) {
            return getListItem(
                listEntity.list[index].movieId,
                "${Config.baseUrl}${listEntity.list[index].cover}",
                listEntity.list[index].movieName);
          },
        ),
      ),
    );
  }
}
