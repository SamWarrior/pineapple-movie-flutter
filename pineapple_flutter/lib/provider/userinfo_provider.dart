import 'package:flutter/cupertino.dart';
import 'package:pineapple_flutter/entity/user_info_entity.dart';

///
/// 状态共享(本项目只有用户信息是需要全局共享得，其他没有)
///
class UserInfoProvider extends ChangeNotifier {
  UserInfoEntity userInfo = UserInfoEntity();

  changeUserinfo(UserInfoEntity newUserInfo) {
    userInfo = newUserInfo;
    print('userinfo已经更改');
    notifyListeners();
  }
}
