import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pineapple_flutter/config/config.dart';
import 'package:pineapple_flutter/pages/splash_page.dart';
import 'package:pineapple_flutter/provider/userinfo_provider.dart';
import 'package:pineapple_flutter/routers/routers.dart';
import 'package:provider/provider.dart';

class MyApplication extends StatelessWidget {
  const MyApplication({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Routes.initRouter();
    LogUtil.init(tag: 'pineapple', isDebug: true, maxLen: 512);
    return ScreenUtilInit(
      designSize: const Size(750, 1500),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return ChangeNotifierProvider<UserInfoProvider>(
          create: (_) => UserInfoProvider(),
          child: MaterialApp(
            title: '熊猫电影-Flutter',
            onGenerateRoute: Routes.router.generator,
            theme: ThemeData(
              primarySwatch: Colors.blue,
              primaryColor: Config.colorMain,
              fontFamily: 'NotoSansSC-Medium',
            ),
            home: const SplashPage(),
            builder: EasyLoading.init(),
          ),
        );
      },
    );
  }
}
