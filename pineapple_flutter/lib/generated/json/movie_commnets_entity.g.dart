import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/movie_commnets_entity.dart';

MovieCommnetsEntity $MovieCommnetsEntityFromJson(Map<String, dynamic> json) {
	final MovieCommnetsEntity movieCommnetsEntity = MovieCommnetsEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		movieCommnetsEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		movieCommnetsEntity.msg = msg;
	}
	final List<MovieCommnetsList>? list = jsonConvert.convertListNotNull<MovieCommnetsList>(json['list']);
	if (list != null) {
		movieCommnetsEntity.list = list;
	}
	return movieCommnetsEntity;
}

Map<String, dynamic> $MovieCommnetsEntityToJson(MovieCommnetsEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	return data;
}

MovieCommnetsList $MovieCommnetsListFromJson(Map<String, dynamic> json) {
	final MovieCommnetsList movieCommnetsList = MovieCommnetsList();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		movieCommnetsList.sId = sId;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		movieCommnetsList.movieId = movieId;
	}
	final String? customid = jsonConvert.convert<String>(json['customid']);
	if (customid != null) {
		movieCommnetsList.customid = customid;
	}
	final String? content = jsonConvert.convert<String>(json['content']);
	if (content != null) {
		movieCommnetsList.content = content;
	}
	final String? startTime = jsonConvert.convert<String>(json['startTime']);
	if (startTime != null) {
		movieCommnetsList.startTime = startTime;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		movieCommnetsList.iV = iV;
	}
	final String? headicon = jsonConvert.convert<String>(json['headicon']);
	if (headicon != null) {
		movieCommnetsList.headicon = headicon;
	}
	final String? nickname = jsonConvert.convert<String>(json['nickname']);
	if (nickname != null) {
		movieCommnetsList.nickname = nickname;
	}
	return movieCommnetsList;
}

Map<String, dynamic> $MovieCommnetsListToJson(MovieCommnetsList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['movieId'] = entity.movieId;
	data['customid'] = entity.customid;
	data['content'] = entity.content;
	data['startTime'] = entity.startTime;
	data['__v'] = entity.iV;
	data['headicon'] = entity.headicon;
	data['nickname'] = entity.nickname;
	return data;
}