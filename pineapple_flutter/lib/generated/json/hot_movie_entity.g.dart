import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/hot_movie_entity.dart';

HotMovieEntity $HotMovieEntityFromJson(Map<String, dynamic> json) {
	final HotMovieEntity hotMovieEntity = HotMovieEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		hotMovieEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		hotMovieEntity.msg = msg;
	}
	final List<HotMovieHots>? hots = jsonConvert.convertListNotNull<HotMovieHots>(json['hots']);
	if (hots != null) {
		hotMovieEntity.hots = hots;
	}
	return hotMovieEntity;
}

Map<String, dynamic> $HotMovieEntityToJson(HotMovieEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['hots'] =  entity.hots.map((v) => v.toJson()).toList();
	return data;
}

HotMovieHots $HotMovieHotsFromJson(Map<String, dynamic> json) {
	final HotMovieHots hotMovieHots = HotMovieHots();
	final double? score = jsonConvert.convert<double>(json['score']);
	if (score != null) {
		hotMovieHots.score = score;
	}
	final int? counts = jsonConvert.convert<int>(json['counts']);
	if (counts != null) {
		hotMovieHots.counts = counts;
	}
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		hotMovieHots.sId = sId;
	}
	final List<HotMovieHotsPlotsWords>? plotsWords = jsonConvert.convertListNotNull<HotMovieHotsPlotsWords>(json['plotsWords']);
	if (plotsWords != null) {
		hotMovieHots.plotsWords = plotsWords;
	}
	final List<HotMovieHotsPlotsImg>? plotsImg = jsonConvert.convertListNotNull<HotMovieHotsPlotsImg>(json['plotsImg']);
	if (plotsImg != null) {
		hotMovieHots.plotsImg = plotsImg;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		hotMovieHots.movieId = movieId;
	}
	final String? movieName = jsonConvert.convert<String>(json['movieName']);
	if (movieName != null) {
		hotMovieHots.movieName = movieName;
	}
	final String? originaName = jsonConvert.convert<String>(json['originaName']);
	if (originaName != null) {
		hotMovieHots.originaName = originaName;
	}
	final String? info = jsonConvert.convert<String>(json['info']);
	if (info != null) {
		hotMovieHots.info = info;
	}
	final String? releaseDate = jsonConvert.convert<String>(json['releaseDate']);
	if (releaseDate != null) {
		hotMovieHots.releaseDate = releaseDate;
	}
	final String? totalTime = jsonConvert.convert<String>(json['totalTime']);
	if (totalTime != null) {
		hotMovieHots.totalTime = totalTime;
	}
	final String? movieUrl = jsonConvert.convert<String>(json['movieUrl']);
	if (movieUrl != null) {
		hotMovieHots.movieUrl = movieUrl;
	}
	final String? plotDesc = jsonConvert.convert<String>(json['plotDesc']);
	if (plotDesc != null) {
		hotMovieHots.plotDesc = plotDesc;
	}
	final String? cover = jsonConvert.convert<String>(json['cover']);
	if (cover != null) {
		hotMovieHots.cover = cover;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		hotMovieHots.iV = iV;
	}
	return hotMovieHots;
}

Map<String, dynamic> $HotMovieHotsToJson(HotMovieHots entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['score'] = entity.score;
	data['counts'] = entity.counts;
	data['_id'] = entity.sId;
	data['plotsWords'] =  entity.plotsWords.map((v) => v.toJson()).toList();
	data['plotsImg'] =  entity.plotsImg.map((v) => v.toJson()).toList();
	data['movieId'] = entity.movieId;
	data['movieName'] = entity.movieName;
	data['originaName'] = entity.originaName;
	data['info'] = entity.info;
	data['releaseDate'] = entity.releaseDate;
	data['totalTime'] = entity.totalTime;
	data['movieUrl'] = entity.movieUrl;
	data['plotDesc'] = entity.plotDesc;
	data['cover'] = entity.cover;
	data['__v'] = entity.iV;
	return data;
}

HotMovieHotsPlotsWords $HotMovieHotsPlotsWordsFromJson(Map<String, dynamic> json) {
	final HotMovieHotsPlotsWords hotMovieHotsPlotsWords = HotMovieHotsPlotsWords();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		hotMovieHotsPlotsWords.sId = sId;
	}
	final String? imgUrl = jsonConvert.convert<String>(json['imgUrl']);
	if (imgUrl != null) {
		hotMovieHotsPlotsWords.imgUrl = imgUrl;
	}
	final String? actorRole = jsonConvert.convert<String>(json['actorRole']);
	if (actorRole != null) {
		hotMovieHotsPlotsWords.actorRole = actorRole;
	}
	final String? autor = jsonConvert.convert<String>(json['autor']);
	if (autor != null) {
		hotMovieHotsPlotsWords.autor = autor;
	}
	return hotMovieHotsPlotsWords;
}

Map<String, dynamic> $HotMovieHotsPlotsWordsToJson(HotMovieHotsPlotsWords entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['imgUrl'] = entity.imgUrl;
	data['actorRole'] = entity.actorRole;
	data['autor'] = entity.autor;
	return data;
}

HotMovieHotsPlotsImg $HotMovieHotsPlotsImgFromJson(Map<String, dynamic> json) {
	final HotMovieHotsPlotsImg hotMovieHotsPlotsImg = HotMovieHotsPlotsImg();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		hotMovieHotsPlotsImg.sId = sId;
	}
	final String? imgUrl = jsonConvert.convert<String>(json['imgUrl']);
	if (imgUrl != null) {
		hotMovieHotsPlotsImg.imgUrl = imgUrl;
	}
	return hotMovieHotsPlotsImg;
}

Map<String, dynamic> $HotMovieHotsPlotsImgToJson(HotMovieHotsPlotsImg entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['imgUrl'] = entity.imgUrl;
	return data;
}