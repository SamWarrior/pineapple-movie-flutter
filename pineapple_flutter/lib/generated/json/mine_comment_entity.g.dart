import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/mine_comment_entity.dart';

MineCommentEntity $MineCommentEntityFromJson(Map<String, dynamic> json) {
	final MineCommentEntity mineCommentEntity = MineCommentEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		mineCommentEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		mineCommentEntity.msg = msg;
	}
	final List<MineCommentList>? list = jsonConvert.convertListNotNull<MineCommentList>(json['list']);
	if (list != null) {
		mineCommentEntity.list = list;
	}
	return mineCommentEntity;
}

Map<String, dynamic> $MineCommentEntityToJson(MineCommentEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	return data;
}

MineCommentList $MineCommentListFromJson(Map<String, dynamic> json) {
	final MineCommentList mineCommentList = MineCommentList();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		mineCommentList.sId = sId;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		mineCommentList.movieId = movieId;
	}
	final String? customid = jsonConvert.convert<String>(json['customid']);
	if (customid != null) {
		mineCommentList.customid = customid;
	}
	final String? content = jsonConvert.convert<String>(json['content']);
	if (content != null) {
		mineCommentList.content = content;
	}
	final String? startTime = jsonConvert.convert<String>(json['startTime']);
	if (startTime != null) {
		mineCommentList.startTime = startTime;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		mineCommentList.iV = iV;
	}
	final String? movieName = jsonConvert.convert<String>(json['movieName']);
	if (movieName != null) {
		mineCommentList.movieName = movieName;
	}
	final String? cover = jsonConvert.convert<String>(json['cover']);
	if (cover != null) {
		mineCommentList.cover = cover;
	}
	return mineCommentList;
}

Map<String, dynamic> $MineCommentListToJson(MineCommentList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['movieId'] = entity.movieId;
	data['customid'] = entity.customid;
	data['content'] = entity.content;
	data['startTime'] = entity.startTime;
	data['__v'] = entity.iV;
	data['movieName'] = entity.movieName;
	data['cover'] = entity.cover;
	return data;
}