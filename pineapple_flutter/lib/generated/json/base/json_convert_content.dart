// ignore_for_file: non_constant_identifier_names
// ignore_for_file: camel_case_types
// ignore_for_file: prefer_single_quotes

// This file is automatically generated. DO NOT EDIT, all your changes would be lost.
import 'package:flutter/material.dart' show debugPrint;
import 'package:pineapple_flutter/entity/banner_entity.dart';
import 'package:pineapple_flutter/entity/hot_movie_entity.dart';
import 'package:pineapple_flutter/entity/mine_about_entity.dart';
import 'package:pineapple_flutter/entity/mine_collect_entity.dart';
import 'package:pineapple_flutter/entity/mine_comment_entity.dart';
import 'package:pineapple_flutter/entity/mine_message_entity.dart';
import 'package:pineapple_flutter/entity/movie_commnets_entity.dart';
import 'package:pineapple_flutter/entity/movie_detail_entity.dart';
import 'package:pineapple_flutter/entity/movie_list_entity.dart';
import 'package:pineapple_flutter/entity/trailer_entity.dart';
import 'package:pineapple_flutter/entity/user_info_entity.dart';

JsonConvert jsonConvert = JsonConvert();
typedef JsonConvertFunction<T> = T Function(Map<String, dynamic> json);

class JsonConvert {
	static final Map<String, JsonConvertFunction> _convertFuncMap = {
		(BannerEntity).toString(): BannerEntity.fromJson,
		(BannerList).toString(): BannerList.fromJson,
		(HotMovieEntity).toString(): HotMovieEntity.fromJson,
		(HotMovieHots).toString(): HotMovieHots.fromJson,
		(HotMovieHotsPlotsWords).toString(): HotMovieHotsPlotsWords.fromJson,
		(HotMovieHotsPlotsImg).toString(): HotMovieHotsPlotsImg.fromJson,
		(MineAboutEntity).toString(): MineAboutEntity.fromJson,
		(MineAboutData).toString(): MineAboutData.fromJson,
		(MineCollectEntity).toString(): MineCollectEntity.fromJson,
		(MineCollectList).toString(): MineCollectList.fromJson,
		(MineCommentEntity).toString(): MineCommentEntity.fromJson,
		(MineCommentList).toString(): MineCommentList.fromJson,
		(MineMessageEntity).toString(): MineMessageEntity.fromJson,
		(MineMessageList).toString(): MineMessageList.fromJson,
		(MovieCommnetsEntity).toString(): MovieCommnetsEntity.fromJson,
		(MovieCommnetsList).toString(): MovieCommnetsList.fromJson,
		(MovieDetailEntity).toString(): MovieDetailEntity.fromJson,
		(MovieDetailItem).toString(): MovieDetailItem.fromJson,
		(MovieDetailItemPlotsWords).toString(): MovieDetailItemPlotsWords.fromJson,
		(MovieDetailItemPlotsImg).toString(): MovieDetailItemPlotsImg.fromJson,
		(MovieListEntity).toString(): MovieListEntity.fromJson,
		(MovieListList).toString(): MovieListList.fromJson,
		(MovieListListPlotsWords).toString(): MovieListListPlotsWords.fromJson,
		(MovieListListPlotsImg).toString(): MovieListListPlotsImg.fromJson,
		(TrailerEntity).toString(): TrailerEntity.fromJson,
		(TrailerList).toString(): TrailerList.fromJson,
		(UserInfoEntity).toString(): UserInfoEntity.fromJson,
		(UserInfoUser).toString(): UserInfoUser.fromJson,
	};

  T? convert<T>(dynamic value) {
    if (value == null) {
      return null;
    }
    return asT<T>(value);
  }

  List<T?>? convertList<T>(List<dynamic>? value) {
    if (value == null) {
      return null;
    }
    try {
      return value.map((dynamic e) => asT<T>(e)).toList();
    } catch (e, stackTrace) {
      debugPrint('asT<$T> $e $stackTrace');
      return <T>[];
    }
  }

  List<T>? convertListNotNull<T>(dynamic value) {
    if (value == null) {
      return null;
    }
    try {
      return (value as List<dynamic>).map((dynamic e) => asT<T>(e)!).toList();
    } catch (e, stackTrace) {
      debugPrint('asT<$T> $e $stackTrace');
      return <T>[];
    }
  }

  T? asT<T extends Object?>(dynamic value) {
    if (value is T) {
      return value;
    }
    final String type = T.toString();
    try {
      final String valueS = value.toString();
      if (type == "String") {
        return valueS as T;
      } else if (type == "int") {
        final int? intValue = int.tryParse(valueS);
        if (intValue == null) {
          return double.tryParse(valueS)?.toInt() as T?;
        } else {
          return intValue as T;
        }
      } else if (type == "double") {
        return double.parse(valueS) as T;
      } else if (type == "DateTime") {
        return DateTime.parse(valueS) as T;
      } else if (type == "bool") {
        if (valueS == '0' || valueS == '1') {
          return (valueS == '1') as T;
        }
        return (valueS == 'true') as T;
      } else if (type == "Map" || type.startsWith("Map<")) {
        return value as T;
      } else {
        if (_convertFuncMap.containsKey(type)) {
          return _convertFuncMap[type]!(value) as T;
        } else {
          throw UnimplementedError('$type unimplemented');
        }
      }
    } catch (e, stackTrace) {
      debugPrint('asT<$T> $e $stackTrace');
      return null;
    }
  }

	//list is returned by type
	static M? _getListChildType<M>(List<Map<String, dynamic>> data) {
		if(<BannerEntity>[] is M){
			return data.map<BannerEntity>((Map<String, dynamic> e) => BannerEntity.fromJson(e)).toList() as M;
		}
		if(<BannerList>[] is M){
			return data.map<BannerList>((Map<String, dynamic> e) => BannerList.fromJson(e)).toList() as M;
		}
		if(<HotMovieEntity>[] is M){
			return data.map<HotMovieEntity>((Map<String, dynamic> e) => HotMovieEntity.fromJson(e)).toList() as M;
		}
		if(<HotMovieHots>[] is M){
			return data.map<HotMovieHots>((Map<String, dynamic> e) => HotMovieHots.fromJson(e)).toList() as M;
		}
		if(<HotMovieHotsPlotsWords>[] is M){
			return data.map<HotMovieHotsPlotsWords>((Map<String, dynamic> e) => HotMovieHotsPlotsWords.fromJson(e)).toList() as M;
		}
		if(<HotMovieHotsPlotsImg>[] is M){
			return data.map<HotMovieHotsPlotsImg>((Map<String, dynamic> e) => HotMovieHotsPlotsImg.fromJson(e)).toList() as M;
		}
		if(<MineAboutEntity>[] is M){
			return data.map<MineAboutEntity>((Map<String, dynamic> e) => MineAboutEntity.fromJson(e)).toList() as M;
		}
		if(<MineAboutData>[] is M){
			return data.map<MineAboutData>((Map<String, dynamic> e) => MineAboutData.fromJson(e)).toList() as M;
		}
		if(<MineCollectEntity>[] is M){
			return data.map<MineCollectEntity>((Map<String, dynamic> e) => MineCollectEntity.fromJson(e)).toList() as M;
		}
		if(<MineCollectList>[] is M){
			return data.map<MineCollectList>((Map<String, dynamic> e) => MineCollectList.fromJson(e)).toList() as M;
		}
		if(<MineCommentEntity>[] is M){
			return data.map<MineCommentEntity>((Map<String, dynamic> e) => MineCommentEntity.fromJson(e)).toList() as M;
		}
		if(<MineCommentList>[] is M){
			return data.map<MineCommentList>((Map<String, dynamic> e) => MineCommentList.fromJson(e)).toList() as M;
		}
		if(<MineMessageEntity>[] is M){
			return data.map<MineMessageEntity>((Map<String, dynamic> e) => MineMessageEntity.fromJson(e)).toList() as M;
		}
		if(<MineMessageList>[] is M){
			return data.map<MineMessageList>((Map<String, dynamic> e) => MineMessageList.fromJson(e)).toList() as M;
		}
		if(<MovieCommnetsEntity>[] is M){
			return data.map<MovieCommnetsEntity>((Map<String, dynamic> e) => MovieCommnetsEntity.fromJson(e)).toList() as M;
		}
		if(<MovieCommnetsList>[] is M){
			return data.map<MovieCommnetsList>((Map<String, dynamic> e) => MovieCommnetsList.fromJson(e)).toList() as M;
		}
		if(<MovieDetailEntity>[] is M){
			return data.map<MovieDetailEntity>((Map<String, dynamic> e) => MovieDetailEntity.fromJson(e)).toList() as M;
		}
		if(<MovieDetailItem>[] is M){
			return data.map<MovieDetailItem>((Map<String, dynamic> e) => MovieDetailItem.fromJson(e)).toList() as M;
		}
		if(<MovieDetailItemPlotsWords>[] is M){
			return data.map<MovieDetailItemPlotsWords>((Map<String, dynamic> e) => MovieDetailItemPlotsWords.fromJson(e)).toList() as M;
		}
		if(<MovieDetailItemPlotsImg>[] is M){
			return data.map<MovieDetailItemPlotsImg>((Map<String, dynamic> e) => MovieDetailItemPlotsImg.fromJson(e)).toList() as M;
		}
		if(<MovieListEntity>[] is M){
			return data.map<MovieListEntity>((Map<String, dynamic> e) => MovieListEntity.fromJson(e)).toList() as M;
		}
		if(<MovieListList>[] is M){
			return data.map<MovieListList>((Map<String, dynamic> e) => MovieListList.fromJson(e)).toList() as M;
		}
		if(<MovieListListPlotsWords>[] is M){
			return data.map<MovieListListPlotsWords>((Map<String, dynamic> e) => MovieListListPlotsWords.fromJson(e)).toList() as M;
		}
		if(<MovieListListPlotsImg>[] is M){
			return data.map<MovieListListPlotsImg>((Map<String, dynamic> e) => MovieListListPlotsImg.fromJson(e)).toList() as M;
		}
		if(<TrailerEntity>[] is M){
			return data.map<TrailerEntity>((Map<String, dynamic> e) => TrailerEntity.fromJson(e)).toList() as M;
		}
		if(<TrailerList>[] is M){
			return data.map<TrailerList>((Map<String, dynamic> e) => TrailerList.fromJson(e)).toList() as M;
		}
		if(<UserInfoEntity>[] is M){
			return data.map<UserInfoEntity>((Map<String, dynamic> e) => UserInfoEntity.fromJson(e)).toList() as M;
		}
		if(<UserInfoUser>[] is M){
			return data.map<UserInfoUser>((Map<String, dynamic> e) => UserInfoUser.fromJson(e)).toList() as M;
		}

		debugPrint("${M.toString()} not found");
	
		return null;
}

	static M? fromJsonAsT<M>(dynamic json) {
		if (json is List) {
			return _getListChildType<M>(json.map((e) => e as Map<String, dynamic>).toList());
		} else {
			return jsonConvert.asT<M>(json);
		}
	}
}