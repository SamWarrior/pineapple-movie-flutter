import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/mine_about_entity.dart';

MineAboutEntity $MineAboutEntityFromJson(Map<String, dynamic> json) {
	final MineAboutEntity mineAboutEntity = MineAboutEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		mineAboutEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		mineAboutEntity.msg = msg;
	}
	final List<MineAboutData>? data = jsonConvert.convertListNotNull<MineAboutData>(json['data']);
	if (data != null) {
		mineAboutEntity.data = data;
	}
	return mineAboutEntity;
}

Map<String, dynamic> $MineAboutEntityToJson(MineAboutEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}

MineAboutData $MineAboutDataFromJson(Map<String, dynamic> json) {
	final MineAboutData mineAboutData = MineAboutData();
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		mineAboutData.title = title;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		mineAboutData.text = text;
	}
	return mineAboutData;
}

Map<String, dynamic> $MineAboutDataToJson(MineAboutData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['title'] = entity.title;
	data['text'] = entity.text;
	return data;
}