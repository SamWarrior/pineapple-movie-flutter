import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/movie_detail_entity.dart';

MovieDetailEntity $MovieDetailEntityFromJson(Map<String, dynamic> json) {
	final MovieDetailEntity movieDetailEntity = MovieDetailEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		movieDetailEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		movieDetailEntity.msg = msg;
	}
	final MovieDetailItem? item = jsonConvert.convert<MovieDetailItem>(json['item']);
	if (item != null) {
		movieDetailEntity.item = item;
	}
	return movieDetailEntity;
}

Map<String, dynamic> $MovieDetailEntityToJson(MovieDetailEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['item'] = entity.item.toJson();
	return data;
}

MovieDetailItem $MovieDetailItemFromJson(Map<String, dynamic> json) {
	final MovieDetailItem movieDetailItem = MovieDetailItem();
	final double? score = jsonConvert.convert<double>(json['score']);
	if (score != null) {
		movieDetailItem.score = score;
	}
	final int? counts = jsonConvert.convert<int>(json['counts']);
	if (counts != null) {
		movieDetailItem.counts = counts;
	}
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		movieDetailItem.sId = sId;
	}
	final List<MovieDetailItemPlotsWords>? plotsWords = jsonConvert.convertListNotNull<MovieDetailItemPlotsWords>(json['plotsWords']);
	if (plotsWords != null) {
		movieDetailItem.plotsWords = plotsWords;
	}
	final List<MovieDetailItemPlotsImg>? plotsImg = jsonConvert.convertListNotNull<MovieDetailItemPlotsImg>(json['plotsImg']);
	if (plotsImg != null) {
		movieDetailItem.plotsImg = plotsImg;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		movieDetailItem.movieId = movieId;
	}
	final String? movieName = jsonConvert.convert<String>(json['movieName']);
	if (movieName != null) {
		movieDetailItem.movieName = movieName;
	}
	final String? originaName = jsonConvert.convert<String>(json['originaName']);
	if (originaName != null) {
		movieDetailItem.originaName = originaName;
	}
	final String? info = jsonConvert.convert<String>(json['info']);
	if (info != null) {
		movieDetailItem.info = info;
	}
	final String? releaseDate = jsonConvert.convert<String>(json['releaseDate']);
	if (releaseDate != null) {
		movieDetailItem.releaseDate = releaseDate;
	}
	final String? totalTime = jsonConvert.convert<String>(json['totalTime']);
	if (totalTime != null) {
		movieDetailItem.totalTime = totalTime;
	}
	final String? movieUrl = jsonConvert.convert<String>(json['movieUrl']);
	if (movieUrl != null) {
		movieDetailItem.movieUrl = movieUrl;
	}
	final String? plotDesc = jsonConvert.convert<String>(json['plotDesc']);
	if (plotDesc != null) {
		movieDetailItem.plotDesc = plotDesc;
	}
	final String? cover = jsonConvert.convert<String>(json['cover']);
	if (cover != null) {
		movieDetailItem.cover = cover;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		movieDetailItem.iV = iV;
	}
	final String? iscollec = jsonConvert.convert<String>(json['iscollec']);
	if (iscollec != null) {
		movieDetailItem.iscollec = iscollec;
	}
	return movieDetailItem;
}

Map<String, dynamic> $MovieDetailItemToJson(MovieDetailItem entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['score'] = entity.score;
	data['counts'] = entity.counts;
	data['_id'] = entity.sId;
	data['plotsWords'] =  entity.plotsWords.map((v) => v.toJson()).toList();
	data['plotsImg'] =  entity.plotsImg.map((v) => v.toJson()).toList();
	data['movieId'] = entity.movieId;
	data['movieName'] = entity.movieName;
	data['originaName'] = entity.originaName;
	data['info'] = entity.info;
	data['releaseDate'] = entity.releaseDate;
	data['totalTime'] = entity.totalTime;
	data['movieUrl'] = entity.movieUrl;
	data['plotDesc'] = entity.plotDesc;
	data['cover'] = entity.cover;
	data['__v'] = entity.iV;
	data['iscollec'] = entity.iscollec;
	return data;
}

MovieDetailItemPlotsWords $MovieDetailItemPlotsWordsFromJson(Map<String, dynamic> json) {
	final MovieDetailItemPlotsWords movieDetailItemPlotsWords = MovieDetailItemPlotsWords();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		movieDetailItemPlotsWords.sId = sId;
	}
	final String? imgUrl = jsonConvert.convert<String>(json['imgUrl']);
	if (imgUrl != null) {
		movieDetailItemPlotsWords.imgUrl = imgUrl;
	}
	final String? autor = jsonConvert.convert<String>(json['autor']);
	if (autor != null) {
		movieDetailItemPlotsWords.autor = autor;
	}
	final String? actorRole = jsonConvert.convert<String>(json['actorRole']);
	if (actorRole != null) {
		movieDetailItemPlotsWords.actorRole = actorRole;
	}
	return movieDetailItemPlotsWords;
}

Map<String, dynamic> $MovieDetailItemPlotsWordsToJson(MovieDetailItemPlotsWords entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['imgUrl'] = entity.imgUrl;
	data['autor'] = entity.autor;
	data['actorRole'] = entity.actorRole;
	return data;
}

MovieDetailItemPlotsImg $MovieDetailItemPlotsImgFromJson(Map<String, dynamic> json) {
	final MovieDetailItemPlotsImg movieDetailItemPlotsImg = MovieDetailItemPlotsImg();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		movieDetailItemPlotsImg.sId = sId;
	}
	final String? imgUrl = jsonConvert.convert<String>(json['imgUrl']);
	if (imgUrl != null) {
		movieDetailItemPlotsImg.imgUrl = imgUrl;
	}
	return movieDetailItemPlotsImg;
}

Map<String, dynamic> $MovieDetailItemPlotsImgToJson(MovieDetailItemPlotsImg entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['imgUrl'] = entity.imgUrl;
	return data;
}