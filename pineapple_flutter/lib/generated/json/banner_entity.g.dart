import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/banner_entity.dart';

BannerEntity $BannerEntityFromJson(Map<String, dynamic> json) {
	final BannerEntity bannerEntity = BannerEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		bannerEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		bannerEntity.msg = msg;
	}
	final List<BannerList>? list = jsonConvert.convertListNotNull<BannerList>(json['list']);
	if (list != null) {
		bannerEntity.list = list;
	}
	return bannerEntity;
}

Map<String, dynamic> $BannerEntityToJson(BannerEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	return data;
}

BannerList $BannerListFromJson(Map<String, dynamic> json) {
	final BannerList bannerList = BannerList();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		bannerList.sId = sId;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		bannerList.movieId = movieId;
	}
	final String? movieName = jsonConvert.convert<String>(json['movieName']);
	if (movieName != null) {
		bannerList.movieName = movieName;
	}
	final String? imageUrl = jsonConvert.convert<String>(json['imageUrl']);
	if (imageUrl != null) {
		bannerList.imageUrl = imageUrl;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		bannerList.iV = iV;
	}
	return bannerList;
}

Map<String, dynamic> $BannerListToJson(BannerList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['movieId'] = entity.movieId;
	data['movieName'] = entity.movieName;
	data['imageUrl'] = entity.imageUrl;
	data['__v'] = entity.iV;
	return data;
}