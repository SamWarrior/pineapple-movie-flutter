import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/mine_message_entity.dart';

MineMessageEntity $MineMessageEntityFromJson(Map<String, dynamic> json) {
	final MineMessageEntity mineMessageEntity = MineMessageEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		mineMessageEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		mineMessageEntity.msg = msg;
	}
	final List<MineMessageList>? list = jsonConvert.convertListNotNull<MineMessageList>(json['list']);
	if (list != null) {
		mineMessageEntity.list = list;
	}
	return mineMessageEntity;
}

Map<String, dynamic> $MineMessageEntityToJson(MineMessageEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	return data;
}

MineMessageList $MineMessageListFromJson(Map<String, dynamic> json) {
	final MineMessageList mineMessageList = MineMessageList();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		mineMessageList.sId = sId;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		mineMessageList.title = title;
	}
	final String? content = jsonConvert.convert<String>(json['content']);
	if (content != null) {
		mineMessageList.content = content;
	}
	final String? startTime = jsonConvert.convert<String>(json['startTime']);
	if (startTime != null) {
		mineMessageList.startTime = startTime;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		mineMessageList.iV = iV;
	}
	return mineMessageList;
}

Map<String, dynamic> $MineMessageListToJson(MineMessageList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['title'] = entity.title;
	data['content'] = entity.content;
	data['startTime'] = entity.startTime;
	data['__v'] = entity.iV;
	return data;
}