import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/movie_list_entity.dart';

MovieListEntity $MovieListEntityFromJson(Map<String, dynamic> json) {
	final MovieListEntity movieListEntity = MovieListEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		movieListEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		movieListEntity.msg = msg;
	}
	final List<MovieListList>? list = jsonConvert.convertListNotNull<MovieListList>(json['list']);
	if (list != null) {
		movieListEntity.list = list;
	}
	return movieListEntity;
}

Map<String, dynamic> $MovieListEntityToJson(MovieListEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	return data;
}

MovieListList $MovieListListFromJson(Map<String, dynamic> json) {
	final MovieListList movieListList = MovieListList();
	final double? score = jsonConvert.convert<double>(json['score']);
	if (score != null) {
		movieListList.score = score;
	}
	final int? counts = jsonConvert.convert<int>(json['counts']);
	if (counts != null) {
		movieListList.counts = counts;
	}
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		movieListList.sId = sId;
	}
	final List<MovieListListPlotsWords>? plotsWords = jsonConvert.convertListNotNull<MovieListListPlotsWords>(json['plotsWords']);
	if (plotsWords != null) {
		movieListList.plotsWords = plotsWords;
	}
	final List<MovieListListPlotsImg>? plotsImg = jsonConvert.convertListNotNull<MovieListListPlotsImg>(json['plotsImg']);
	if (plotsImg != null) {
		movieListList.plotsImg = plotsImg;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		movieListList.movieId = movieId;
	}
	final String? movieName = jsonConvert.convert<String>(json['movieName']);
	if (movieName != null) {
		movieListList.movieName = movieName;
	}
	final String? originaName = jsonConvert.convert<String>(json['originaName']);
	if (originaName != null) {
		movieListList.originaName = originaName;
	}
	final String? info = jsonConvert.convert<String>(json['info']);
	if (info != null) {
		movieListList.info = info;
	}
	final String? releaseDate = jsonConvert.convert<String>(json['releaseDate']);
	if (releaseDate != null) {
		movieListList.releaseDate = releaseDate;
	}
	final String? totalTime = jsonConvert.convert<String>(json['totalTime']);
	if (totalTime != null) {
		movieListList.totalTime = totalTime;
	}
	final String? movieUrl = jsonConvert.convert<String>(json['movieUrl']);
	if (movieUrl != null) {
		movieListList.movieUrl = movieUrl;
	}
	final String? plotDesc = jsonConvert.convert<String>(json['plotDesc']);
	if (plotDesc != null) {
		movieListList.plotDesc = plotDesc;
	}
	final String? cover = jsonConvert.convert<String>(json['cover']);
	if (cover != null) {
		movieListList.cover = cover;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		movieListList.iV = iV;
	}
	return movieListList;
}

Map<String, dynamic> $MovieListListToJson(MovieListList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['score'] = entity.score;
	data['counts'] = entity.counts;
	data['_id'] = entity.sId;
	data['plotsWords'] =  entity.plotsWords.map((v) => v.toJson()).toList();
	data['plotsImg'] =  entity.plotsImg.map((v) => v.toJson()).toList();
	data['movieId'] = entity.movieId;
	data['movieName'] = entity.movieName;
	data['originaName'] = entity.originaName;
	data['info'] = entity.info;
	data['releaseDate'] = entity.releaseDate;
	data['totalTime'] = entity.totalTime;
	data['movieUrl'] = entity.movieUrl;
	data['plotDesc'] = entity.plotDesc;
	data['cover'] = entity.cover;
	data['__v'] = entity.iV;
	return data;
}

MovieListListPlotsWords $MovieListListPlotsWordsFromJson(Map<String, dynamic> json) {
	final MovieListListPlotsWords movieListListPlotsWords = MovieListListPlotsWords();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		movieListListPlotsWords.sId = sId;
	}
	final String? imgUrl = jsonConvert.convert<String>(json['imgUrl']);
	if (imgUrl != null) {
		movieListListPlotsWords.imgUrl = imgUrl;
	}
	final String? autor = jsonConvert.convert<String>(json['autor']);
	if (autor != null) {
		movieListListPlotsWords.autor = autor;
	}
	final String? actorRole = jsonConvert.convert<String>(json['actorRole']);
	if (actorRole != null) {
		movieListListPlotsWords.actorRole = actorRole;
	}
	return movieListListPlotsWords;
}

Map<String, dynamic> $MovieListListPlotsWordsToJson(MovieListListPlotsWords entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['imgUrl'] = entity.imgUrl;
	data['autor'] = entity.autor;
	data['actorRole'] = entity.actorRole;
	return data;
}

MovieListListPlotsImg $MovieListListPlotsImgFromJson(Map<String, dynamic> json) {
	final MovieListListPlotsImg movieListListPlotsImg = MovieListListPlotsImg();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		movieListListPlotsImg.sId = sId;
	}
	final String? imgUrl = jsonConvert.convert<String>(json['imgUrl']);
	if (imgUrl != null) {
		movieListListPlotsImg.imgUrl = imgUrl;
	}
	return movieListListPlotsImg;
}

Map<String, dynamic> $MovieListListPlotsImgToJson(MovieListListPlotsImg entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['imgUrl'] = entity.imgUrl;
	return data;
}