import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/user_info_entity.dart';

UserInfoEntity $UserInfoEntityFromJson(Map<String, dynamic> json) {
	final UserInfoEntity userInfoEntity = UserInfoEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		userInfoEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		userInfoEntity.msg = msg;
	}
	final UserInfoUser? user = jsonConvert.convert<UserInfoUser>(json['user']);
	if (user != null) {
		userInfoEntity.user = user;
	}
	return userInfoEntity;
}

Map<String, dynamic> $UserInfoEntityToJson(UserInfoEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['user'] = entity.user.toJson();
	return data;
}

UserInfoUser $UserInfoUserFromJson(Map<String, dynamic> json) {
	final UserInfoUser userInfoUser = UserInfoUser();
	final String? headicon = jsonConvert.convert<String>(json['headicon']);
	if (headicon != null) {
		userInfoUser.headicon = headicon;
	}
	final String? nickname = jsonConvert.convert<String>(json['nickname']);
	if (nickname != null) {
		userInfoUser.nickname = nickname;
	}
	final String? address = jsonConvert.convert<String>(json['address']);
	if (address != null) {
		userInfoUser.address = address;
	}
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		userInfoUser.sId = sId;
	}
	final String? username = jsonConvert.convert<String>(json['username']);
	if (username != null) {
		userInfoUser.username = username;
	}
	final String? password = jsonConvert.convert<String>(json['password']);
	if (password != null) {
		userInfoUser.password = password;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		userInfoUser.iV = iV;
	}
	return userInfoUser;
}

Map<String, dynamic> $UserInfoUserToJson(UserInfoUser entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['headicon'] = entity.headicon;
	data['nickname'] = entity.nickname;
	data['address'] = entity.address;
	data['_id'] = entity.sId;
	data['username'] = entity.username;
	data['password'] = entity.password;
	data['__v'] = entity.iV;
	return data;
}