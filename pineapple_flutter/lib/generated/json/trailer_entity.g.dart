import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/trailer_entity.dart';

TrailerEntity $TrailerEntityFromJson(Map<String, dynamic> json) {
	final TrailerEntity trailerEntity = TrailerEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		trailerEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		trailerEntity.msg = msg;
	}
	final List<TrailerList>? list = jsonConvert.convertListNotNull<TrailerList>(json['list']);
	if (list != null) {
		trailerEntity.list = list;
	}
	return trailerEntity;
}

Map<String, dynamic> $TrailerEntityToJson(TrailerEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	return data;
}

TrailerList $TrailerListFromJson(Map<String, dynamic> json) {
	final TrailerList trailerList = TrailerList();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		trailerList.sId = sId;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		trailerList.movieId = movieId;
	}
	final String? movieName = jsonConvert.convert<String>(json['movieName']);
	if (movieName != null) {
		trailerList.movieName = movieName;
	}
	final String? movieUrl = jsonConvert.convert<String>(json['movieUrl']);
	if (movieUrl != null) {
		trailerList.movieUrl = movieUrl;
	}
	final String? cover = jsonConvert.convert<String>(json['cover']);
	if (cover != null) {
		trailerList.cover = cover;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		trailerList.iV = iV;
	}
	return trailerList;
}

Map<String, dynamic> $TrailerListToJson(TrailerList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['movieId'] = entity.movieId;
	data['movieName'] = entity.movieName;
	data['movieUrl'] = entity.movieUrl;
	data['cover'] = entity.cover;
	data['__v'] = entity.iV;
	return data;
}