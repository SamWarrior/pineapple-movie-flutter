import 'package:pineapple_flutter/generated/json/base/json_convert_content.dart';
import 'package:pineapple_flutter/entity/mine_collect_entity.dart';

MineCollectEntity $MineCollectEntityFromJson(Map<String, dynamic> json) {
	final MineCollectEntity mineCollectEntity = MineCollectEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		mineCollectEntity.code = code;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		mineCollectEntity.msg = msg;
	}
	final List<MineCollectList>? list = jsonConvert.convertListNotNull<MineCollectList>(json['list']);
	if (list != null) {
		mineCollectEntity.list = list;
	}
	return mineCollectEntity;
}

Map<String, dynamic> $MineCollectEntityToJson(MineCollectEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	return data;
}

MineCollectList $MineCollectListFromJson(Map<String, dynamic> json) {
	final MineCollectList mineCollectList = MineCollectList();
	final String? sId = jsonConvert.convert<String>(json['_id']);
	if (sId != null) {
		mineCollectList.sId = sId;
	}
	final String? customid = jsonConvert.convert<String>(json['customid']);
	if (customid != null) {
		mineCollectList.customid = customid;
	}
	final String? movieId = jsonConvert.convert<String>(json['movieId']);
	if (movieId != null) {
		mineCollectList.movieId = movieId;
	}
	final String? movieName = jsonConvert.convert<String>(json['movieName']);
	if (movieName != null) {
		mineCollectList.movieName = movieName;
	}
	final String? cover = jsonConvert.convert<String>(json['cover']);
	if (cover != null) {
		mineCollectList.cover = cover;
	}
	final int? iV = jsonConvert.convert<int>(json['__v']);
	if (iV != null) {
		mineCollectList.iV = iV;
	}
	return mineCollectList;
}

Map<String, dynamic> $MineCollectListToJson(MineCollectList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['_id'] = entity.sId;
	data['customid'] = entity.customid;
	data['movieId'] = entity.movieId;
	data['movieName'] = entity.movieName;
	data['cover'] = entity.cover;
	data['__v'] = entity.iV;
	return data;
}